public class UF {

    private int[] parent; // parent[i] = parent of i
    private byte[] rank; // rank[i] = rank of subtree rooted at i (never more than 31)
    private int count; // number of components

    public UF(int n) {
        if (n < 0) throw new IllegalArgumentException();
        count = n;
        parent = new int[n];
        rank = new byte[n];
        for (int i = 0; i < n; i++) {
            parent[i] = i;
            rank[i] = 0;
        }
    }
    public int find(int p) {
        validate(p);
        while (p != parent[p]) {
            parent[p] = parent[parent[p]]; // path compression by halving
            p = parent[p];
        }
        return p;
    }

    /**
     * Returns the number of components.
     *
     * @return the number of components (between {@code 1} and {@code n})
     */
    public int getCount() {
        return count;
    }

    public boolean connected(int p, int q) {
        return find(p) == find(q);
    }

    public void union(int p, int q) {
        int rootP = find(p);
        int rootQ = find(q);
        if (rootP == rootQ)
            return;

        // make root of smaller rank point to root of larger rank
        if (rank[rootP] < rank[rootQ])
            parent[rootP] = rootQ;
        else if (rank[rootP] > rank[rootQ])
            parent[rootQ] = rootP;
        else {
            parent[rootQ] = rootP;
            rank[rootP]++;
        }
        count--;
    }

    // validate that p is a valid index
    private void validate(int p) {
        int n = parent.length;
        if (p < 0 || p >= n) {
            throw new IllegalArgumentException("index " + p + " is not between 0 and " + (n - 1));
        }
    }

    public static void main(String[] args) {
       System.out.println("The test will now commence.\n");
       System.out.println("First, the union structure will be initialized and printed.\n");
       UF unionGraph = new UF(25);
       for(int i = 0; i < unionGraph.getCount(); i++)
       {
        System.out.println("If the " + i + " is within, it will print to the right: "+unionGraph.find(i));
       }
       System.out.println("Now that the union grapg has been initialized to 25, we wil join two of the elements together. For fun, the elements of 10 and 24 will be unioned and the resultant union.");
       System.out.println("This will find the 10 and print it: " + unionGraph.find(10));
       System.out.println("This will find the 24 and print it: " + unionGraph.find(24));
       unionGraph.union(10, 24);
       System.out.println("Element: 10 Parent: " + unionGraph.find(10));
       System.out.println("Element: 24 Parent: " + unionGraph.find(24));
       System.out.println("This will find the 3 and print it: " + unionGraph.find(3));
       System.out.println("This will find the 20 and print it: " + unionGraph.find(20));
       unionGraph.union(3, 20);
       System.out.println("Element: 3 Parent: " + unionGraph.find(3));
       System.out.println("Element: 20 Parent: " + unionGraph.find(20));
       System.out.println("Now that there are two separate groups, they will be unioned together and all found.");
       unionGraph.union(3, 24);
       System.out.println("Element: 10 Parent: " + unionGraph.find(10));
       System.out.println("Element: 24 Parent: " + unionGraph.find(24));
       System.out.println("Element: 3 Parent: " + unionGraph.find(3));
       System.out.println("Element: 20 Parent: " + unionGraph.find(20));

    }
}