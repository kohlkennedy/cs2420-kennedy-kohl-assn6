import java.util.Random;

public class Percolation {

	private boolean[][] opened;
	private int top = 0;
	private int bottom;
	private int size;
	private UF qf;

	/**
	 * Creates N-by-N grid, with all sites blocked.
	 */
	public Percolation(int N) {
		size = N;
		bottom = size * size + 1;
		qf = new UF(size * size + 2);
		opened = new boolean[size][size];
	}

	/**
	 * Opens site (row i, column j) if it is not already.
	 */
	public void open(int i, int j) {
		opened[i - 1][j - 1] = true;
		if (i == 1) {
			qf.union(getQFIndex(i, j), top);
		}
		if (i == size) {
			qf.union(getQFIndex(i, j), bottom);
		}

		if (j > 1 && isOpen(i, j - 1)) {
			qf.union(getQFIndex(i, j), getQFIndex(i, j - 1));
		}
		if (j < size && isOpen(i, j + 1)) {
			qf.union(getQFIndex(i, j), getQFIndex(i, j + 1));
		}
		if (i > 1 && isOpen(i - 1, j)) {
			qf.union(getQFIndex(i, j), getQFIndex(i - 1, j));
		}
		if (i < size && isOpen(i + 1, j)) {
			qf.union(getQFIndex(i, j), getQFIndex(i + 1, j));
		}
	}

	/**
	 * Is site (row i, column j) open?
	 */
	public boolean isOpen(int i, int j) {
		return opened[i - 1][j - 1];
	}

	/**
	 * Is site (row i, column j) full?
	 */
	public boolean isFull(int i, int j) {
		if (0 < i && i <= size && 0 < j && j <= size) {
			return qf.connected(top, getQFIndex(i, j));
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	/**
	 * Does the system percolate?
	 */
	public boolean percolates() {
		return qf.connected(top, bottom);
	}

	private int getQFIndex(int i, int j) {
		return size * (i - 1) + j;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (isFull(i + 1, j + 1))
					sb.append('\u3119');
				else if (opened[i][j])
					sb.append('\u25A0');

				else
					sb.append('\u25A2');
			}
			sb.append('\n');
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		Percolation perky = new Percolation(20);
		double average = 0;
		int i = 0;
		while (i < 2) {
			int name = 1;
			while (!perky.percolates()) {

				for (int j = 0; i < 50; j++) {
					Random random = new Random();
					int randInt1 = random.nextInt(20) + 1;

					int randInt2 = random.nextInt(20) + 1;

					perky.open(randInt1, randInt2);
					name++;
					if (perky.percolates())
						break;
				}
				System.out.println("Percolation #" + name + "\n" + perky.toString() + "\n");
				average += name;
			}
			i++;
		}
		System.out.println("The average amount of percolations required is "  + (average));

	}
}